
#define SIZE_OF_BOMB_LIST 250
#define ROOM_MODELING_DEFAULT 1

struct bomb {
	int enable;
	double x;
	double y;
	double z;
	double speed;
};

struct bomblist {
	struct bomb list[SIZE_OF_BOMB_LIST];
	int end;
	int room;
	int number;
};

enum eDIRECTION { DIRECTION_LEFT,
	DIRECTION_RIGHT, DIRECTION_UP,
	DIRECTION_DOWN, DIRECTION_FORWARD,
	DIRECTION_BACK
};

extern double thetime, flame_step;
extern double window_width;
extern double window_height;

extern double light_x, light_y, light_z;
extern double roomsize_x, roomsize_y, roomsize_z, self_size;
extern double viewdepth, viewrotatex, viewrotatey;
extern double swing_depth, swing_x, swing_y;
extern double hitpoint, lifespan, damage, teapot_time;
extern int crash_flag, tidalwave_flag;
extern int play_level, room_modeling;
extern int point, tidalwave_point;
extern enum eDIRECTION swing_direction;
extern struct bomblist bomblist;
extern struct bomb teapot, self;
