
#include <GL/gl.h>
#include <GL/glut.h>

/* 正方形を格子状に分割して描画する */
void glid (int divx, int divy)
{
	int extcn, intcn;
	glNormal3f (0, 0, 1);
	for (extcn = 0; extcn < divy; extcn++) {
		glBegin (GL_TRIANGLE_STRIP);
		for (intcn = 0; intcn < (divx + 1); intcn++) {
			glVertex2d ((double) intcn / divx - 0.5,
				    (double) extcn / divy - 0.5);
			glVertex2d ((double) intcn / divx - 0.5,
				    (double) (extcn + 1) / divy - 0.5);
		}
		glEnd ();
	}
}


/* 立方体を格子状に分割して描画する */
void glidcube (int div)
{
	glPushMatrix ();
	glTranslatef (0, 0, -0.5);
	glid (div, div);
	glPopMatrix ();
	glPushMatrix ();
	glRotatef (180, 0, 1, 0);
	glTranslatef (0, 0, -0.5);
	glid (div, div);
	glPopMatrix ();
	glPushMatrix ();
	glRotatef (90, 1, 0, 0);
	glTranslatef (0, 0, -0.5);
	glid (div, div);
	glPopMatrix ();
	glPushMatrix ();
	glRotatef (90, -1, 0, 0);
	glTranslatef (0, 0, -0.5);
	glid (div, div);
	glPopMatrix ();
	glPushMatrix ();
	glRotatef (90, 0, 1, 0);
	glTranslatef (0, 0, -0.5);
	glid (div, div);
	glPopMatrix ();
	glPushMatrix ();
	glRotatef (90, 0, -1, 0);
	glTranslatef (0, 0, -0.5);
	glid (div, div);
	glPopMatrix ();
	glPopMatrix ();
}


/* 縁を丸く削った立方体を描画する */
void RoundCube (double margin)
{
	double a = 0.5 - margin;
	glBegin (GL_QUADS);
	glNormal3d (0, 0, -1);
	glVertex3d (-a, a, -0.5);
	glVertex3d (a, a, -0.5);
	glVertex3d (a, -a, -0.5);
	glVertex3d (-a, -a, -0.5);

	glNormal3d (0, 0, 1);
	glVertex3d (-a, -a, 0.5);
	glVertex3d (a, -a, 0.5);
	glVertex3d (a, a, 0.5);
	glVertex3d (-a, a, 0.5);

	glNormal3d (-1, 0, 0);
	glVertex3d (-0.5, -a, a);
	glVertex3d (-0.5, a, a);
	glVertex3d (-0.5, a, -a);
	glVertex3d (-0.5, -a, -a);

	glNormal3d (1, 0, 0);
	glVertex3d (0.5, -a, -a);
	glVertex3d (0.5, a, -a);
	glVertex3d (0.5, a, a);
	glVertex3d (0.5, -a, a);

	glNormal3d (0, -1, 0);
	glVertex3d (a, -0.5, -a);
	glVertex3d (a, -0.5, a);
	glVertex3d (-a, -0.5, a);
	glVertex3d (-a, -0.5, -a);

	glNormal3d (0, 1, 0);
	glVertex3d (-a, 0.5, -a);
	glVertex3d (-a, 0.5, a);
	glVertex3d (a, 0.5, a);
	glVertex3d (a, 0.5, -a);

	//---- 
	glNormal3d (0, 1, 0);
	glVertex3d (-a, 0.5, -a);
	glVertex3d (a, 0.5, -a);
	glNormal3d (0, 0, -1);
	glVertex3d (a, a, -0.5);
	glVertex3d (-a, a, -0.5);

	glNormal3d (0, -1, 0);
	glVertex3d (a, -0.5, -a);
	glVertex3d (-a, -0.5, -a);
	glNormal3d (0, 0, -1);
	glVertex3d (-a, -a, -0.5);
	glVertex3d (a, -a, -0.5);

	glNormal3d (1, 0, 0);
	glVertex3d (0.5, a, -a);
	glVertex3d (0.5, -a, -a);
	glNormal3d (0, 0, -1);
	glVertex3d (a, -a, -0.5);
	glVertex3d (a, a, -0.5);

	glNormal3d (-1, 0, 0);
	glVertex3d (-0.5, -a, -a);
	glVertex3d (-0.5, a, -a);
	glNormal3d (0, 0, -1);
	glVertex3d (-a, a, -0.5);
	glVertex3d (-a, -a, -0.5);
	//---- 
	glNormal3d (0, 1, 0);
	glVertex3d (a, 0.5, a);
	glVertex3d (-a, 0.5, a);
	glNormal3d (0, 0, 1);
	glVertex3d (-a, a, 0.5);
	glVertex3d (a, a, 0.5);

	glNormal3d (0, -1, 0);
	glVertex3d (-a, -0.5, a);
	glVertex3d (a, -0.5, a);
	glNormal3d (0, 0, 1);
	glVertex3d (a, -a, 0.5);
	glVertex3d (-a, -a, 0.5);

	glNormal3d (-1, 0, 0);
	glVertex3d (-0.5, a, a);
	glVertex3d (-0.5, -a, a);
	glNormal3d (0, 0, 1);
	glVertex3d (-a, -a, 0.5);
	glVertex3d (-a, a, 0.5);

	glNormal3d (1, 0, 0);
	glVertex3d (0.5, -a, a);
	glVertex3d (0.5, a, a);
	glNormal3d (0, 0, 1);
	glVertex3d (a, a, 0.5);
	glVertex3d (a, -a, 0.5);
	//---- 
	glNormal3d (0, 1, 0);
	glVertex3d (a, 0.5, -a);
	glVertex3d (a, 0.5, a);
	glNormal3d (1, 0, 0);
	glVertex3d (0.5, a, a);
	glVertex3d (0.5, a, -a);

	glNormal3d (0, -1, 0);
	glVertex3d (-a, -0.5, -a);
	glVertex3d (-a, -0.5, a);
	glNormal3d (-1, 0, 0);
	glVertex3d (-0.5, -a, a);
	glVertex3d (-0.5, -a, -a);

	glNormal3d (1, 0, 0);
	glVertex3d (0.5, -a, -a);
	glVertex3d (0.5, -a, a);
	glNormal3d (0, -1, 0);
	glVertex3d (a, -0.5, a);
	glVertex3d (a, -0.5, -a);

	glNormal3d (-1, 0, 0);
	glVertex3d (-0.5, a, -a);
	glVertex3d (-0.5, a, a);
	glNormal3d (0, 1, 0);
	glVertex3d (-a, 0.5, a);
	glVertex3d (-a, 0.5, -a);
	glEnd ();

	glBegin (GL_TRIANGLES);
	glNormal3d (1, 0, 0);
	glVertex3d (0.5, a, a);
	glNormal3d (0, 1, 0);
	glVertex3d (a, 0.5, a);
	glNormal3d (0, 0, 1);
	glVertex3d (a, a, 0.5);

	glNormal3d (-1, 0, 0);
	glVertex3d (-0.5, -a, a);
	glNormal3d (0, -1, 0);
	glVertex3d (-a, -0.5, a);
	glNormal3d (0, 0, 1);
	glVertex3d (-a, -a, 0.5);

	glNormal3d (1, 0, 0);
	glVertex3d (0.5, -a, -a);
	glNormal3d (0, -1, 0);
	glVertex3d (a, -0.5, -a);
	glNormal3d (0, 0, -1);
	glVertex3d (a, -a, -0.5);

	glNormal3d (-1, 0, 0);
	glVertex3d (-0.5, a, -a);
	glNormal3d (0, 1, 0);
	glVertex3d (-a, 0.5, -a);
	glNormal3d (0, 0, -1);
	glVertex3d (-a, a, -0.5);

	glNormal3d (0, 0, -1);
	glVertex3d (-a, -a, -0.5);
	glNormal3d (0, -1, 0);
	glVertex3d (-a, -0.5, -a);
	glNormal3d (-1, 0, 0);
	glVertex3d (-0.5, -a, -a);

	glNormal3d (0, 0, -1);
	glVertex3d (a, a, -0.5);
	glNormal3d (0, 1, 0);
	glVertex3d (a, 0.5, -a);
	glNormal3d (1, 0, 0);
	glVertex3d (0.5, a, -a);

	glNormal3d (0, 0, 1);
	glVertex3d (-a, a, 0.5);
	glNormal3d (0, 1, 0);
	glVertex3d (-a, 0.5, a);
	glNormal3d (-1, 0, 0);
	glVertex3d (-0.5, a, a);

	glNormal3d (0, 0, 1);
	glVertex3d (a, -a, 0.5);
	glNormal3d (0, -1, 0);
	glVertex3d (a, -0.5, a);
	glNormal3d (1, 0, 0);
	glVertex3d (0.5, -a, a);
	glEnd ();
}
