
#include "skymagic.h"

double self_x, self_y, self_z;
double light_x, light_y, light_z;
double roomsize_x, roomsize_y, roomsize_z, self_size;
double viewdepth, viewrotatex, viewrotatey;
double swing_depth, swing_x, swing_y;
double metatime, timecache, timebackcache;
double hitpoint, lifespan, damage, teapot_time;
int crash_flag, tidalwave_flag;
int play_level, room_modeling;
int point, tidalwave_point;
enum eDIRECTION swing_direction;
struct bomblist bomblist;
struct bomb teapot, self;

extern void time_init (void);

void game_start (int level)
{
	time_init ();
	play_level = level;
	self.x = 0;
	self.y = 0;
	self.z = 2;
	bomblist.end = 0;
	bomblist.room = 0;
	bomblist.number = 0;
	crash_flag = 0;
	tidalwave_flag = 0;
	teapot.enable = 0;
	teapot_time = metatime = 0;
	hitpoint = 1.0;
	damage = 0;
	if (play_level == 1) {
		damage = 1.0;
	}
	point = 0;
	if (play_level == 1) {
		point = 1000;
	}
	tidalwave_point = point;
}

void viewpoint_reset ()
{
	viewdepth = 11;
	viewrotatex = 30;
	viewrotatey = -8;
	swing_depth = swing_x = swing_y = 0;
	swing_direction = DIRECTION_RIGHT;
}

void initialize (void)
{
	timebackcache = timecache = 0;
	light_x = 0;
	light_y = 0;
	light_z = -2;
	roomsize_x = 4;
	roomsize_y = 2.5;
	roomsize_z = 6;
	self.speed = 3.00;
	self_size = 0.6;
	lifespan = 1.5;
	room_modeling = ROOM_MODELING_DEFAULT;
	viewpoint_reset ();
	game_start (0);
}
