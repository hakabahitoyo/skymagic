
#define TITLE "Skymagic"

#include <stdio.h>
#include <stdlib.h>
#include <SDL/SDL.h>
#include <GL/glut.h>
#include "skymagic.h"

double thetime = 0;
double auxtime = 0;
double flame_step = 0;
SDL_Surface *surface;

extern void opengl_expose (void);
extern void timer_flame (void);
extern void initialize (void);
extern double wrap_time (void);
extern void key_input (void);
extern void keyevent (SDL_KeyboardEvent *key);

double window_width = 800;
double window_height = 600;

void print_description (void) {
	printf ("\n");
	printf ("Move red cube:      D, F, J, K, I, L\n");
	printf ("Fast move:          Shift + D, F, J, K, I, L\n");
	printf ("Move view point:    Ctrl + D, F, J, K, I, L\n");
	printf ("Swing view point 1: Space\n");
	printf ("Swing view point 2: Alt + D, F, J, K, I, L\n");
	printf ("Game start:         Enter\n");
	printf ("Expert mode:        Shift + Enter\n");
	printf ("Wire flame:         Ctrl + W\n");
	printf ("Solid:              Ctrl + S\n");
	printf ("Reset view point:   Ctrl + Space\n");
	printf ("Give up:            Ctrl + Shift + HOME\n");
	printf ("Quit:               Ctrl + Shift + End\n");
	printf ("\n");
}

void time_init (void)
{
	auxtime = wrap_time ();
	thetime = 0;
	srand (auxtime * 1000);
}

void time_set (void) {
	double newtime;
	newtime = wrap_time();
	flame_step = 0;
	if (auxtime < newtime) {
		flame_step = newtime - auxtime;
	}
	auxtime = newtime;
	thetime += flame_step;
}

#ifdef __MINGW32__
int APIENTRY WinMain (HINSTANCE a, HINSTANCE b, LPSTR c,int d)
{
	main (0, NULL);
	return 0;
}
#endif

int main (int argc, char **argv)
{
	SDL_Event event;
	int flag;
	print_description ();
	glutInit (&argc, argv);
	flag = SDL_Init (SDL_INIT_VIDEO);
	if (flag < 0) {
		fprintf (stderr, "SDL initialize Hum! \n");
	}
	atexit (SDL_Quit);
	SDL_GL_SetAttribute( SDL_GL_DOUBLEBUFFER, 1 );
	surface = SDL_SetVideoMode
		(window_width, window_height, 0, SDL_OPENGL | SDL_RESIZABLE);
	if (surface == NULL) {
		fprintf (stderr, "SDL Video initialize Ha!\n");
	}
	SDL_WM_SetCaption (TITLE, NULL);
	initialize();
	for (;;) {
		flag = SDL_PollEvent (&event);
		if (0 < flag) {
			switch (event.type) {
			case SDL_QUIT:
				exit (0);
				break;
			case SDL_KEYDOWN:
				keyevent (&(event.key));
				break;
			case SDL_VIDEORESIZE:
				window_width = event.resize.w;
				window_height = event.resize.h;
				surface = SDL_SetVideoMode
					(window_width, window_height, 0,
					SDL_OPENGL | SDL_RESIZABLE);
				break;
			default:
				break;
			}
		} else {
			time_set();
			key_input ();
			timer_flame ();
			opengl_expose ();
			SDL_GL_SwapBuffers ();
		}
	}
	return 1;
}

