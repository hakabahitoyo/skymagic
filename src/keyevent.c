
#include <stdio.h>
#include <stdlib.h>
#include <SDL/SDL.h>
#include "skymagic.h"

extern void viewpoint_reset ();
extern void game_start (int level);
extern SDL_Surface *surface;

void keyevent (SDL_KeyboardEvent *key_event)
{
	SDLKey sym;
	SDLMod mod;
	sym = key_event->keysym.sym;
	mod = key_event->keysym.mod;
	switch (sym) {
	case SDLK_RETURN:
		if (hitpoint < 0) {
			mod & KMOD_SHIFT? game_start (1): game_start (0);
		}
		break;
	case SDLK_HOME:
		if ((mod & KMOD_SHIFT) && (mod & KMOD_CTRL)) {
			hitpoint = -1;
		}
		break;
	case SDLK_END:
		if ((mod & KMOD_SHIFT) && (mod & KMOD_CTRL)) {
			exit (1);
		}
		break;
	case SDLK_SPACE:
		if (mod & KMOD_CTRL) {
			viewpoint_reset ();
			if (hitpoint < 0) {
				room_modeling = ROOM_MODELING_DEFAULT;
			}
		}
		break;
	case SDLK_s:
		if (mod & KMOD_CTRL) {
			room_modeling = 1;
		}
		break;
	case SDLK_w:
		if (mod & KMOD_CTRL) {
			room_modeling = 0;
		}
		break;
	default:
		break;
	}
}


