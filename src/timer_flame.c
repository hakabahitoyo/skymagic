
#include <stdio.h>
#include <stdlib.h>
#include "skymagic.h"
#define ROTATE_SPEED 30
#define DEPTH_SPEED 6

double max (double a, double b)
{
	return (a < b ? b : a);
}

double min (double a, double b)
{
	return (a < b ? a : b);
}

double rn ()
{
	double result;
	result = (double) rand () / (double) RAND_MAX;
	return result;
}

int crash (struct bomb a, struct bomb b, double distance)
{
	return (a.x - b.x) * (a.x - b.x) + (a.y - b.y) * (a.y - b.y) +
	    (a.z - b.z) * (a.z - b.z) < distance * distance;
}

void timer_flame (void)
{
	struct bomb bomb;
	struct bomb newbomb;
	int flag;
	int bomblot;
	double speed;
	double metatime;

	metatime = thetime;
	if (point < 500) {
		speed = ((double) point / 1000 + 0.50) * self.speed;
	} else {
		speed = ((double) (point - 500) / 2000 + 1.0) * self.speed;
	} if (250 <= point - tidalwave_point && tidalwave_flag == 0) {
		tidalwave_flag = 1;
		tidalwave_point += 250;	/* いろいろ考えた結果 */
	}

	int cn;
	for (cn = 0; cn < bomblist.end; cn++) {
		if (bomblist.list[cn].enable == 1) {
			bomblist.list[cn].z +=
			    bomblist.list[cn].speed * flame_step;
			if (bomblist.list[cn].z > roomsize_z / 2) {
				bomblist.number--;
				bomblist.list[cn].enable = 0;
				bomblist.room = min (bomblist.room, cn);
				if (cn == bomblist.end - 1) {
					bomblist.end = cn;
				}
				if (bomblist.number <= 2
				    && tidalwave_flag == 1) {
					tidalwave_flag = 2;
				}
				if (hitpoint >= 0 && crash_flag == 0) {
					point += (1.0 == hitpoint ? 2 : 1);
				}
			}
		}
	}
	bomblot = (int) (damage * 8) + 16;
	if (tidalwave_flag == 1) {
		bomblot = 0;
	}
	if (tidalwave_flag == 2) {
		bomblot = (int) (2.5 * 16);
	}
	for (; bomblist.number < bomblot;) {
		newbomb.x = (rn () - 0.5) * roomsize_x;
		newbomb.y = (rn () - 0.5) * roomsize_y;
		newbomb.z = -roomsize_z / 2;
		newbomb.speed = (rn () * 0.5 + 0.25) * speed;
		if (tidalwave_flag == 2) {
			newbomb.speed = (rn () * 0.25 + 0.5) * speed;
		}
		newbomb.enable = 1;
		if (bomblist.room < SIZE_OF_BOMB_LIST) {
			bomblist.number++;
			bomblist.list[bomblist.room] = newbomb;
			if (bomblist.room == bomblist.end) {
				bomblist.end++;
			}
			for (cn = bomblist.room; cn < bomblist.end; cn++) {
				if (bomblist.list[cn].enable == 0) {
					break;
				}
			}
			bomblist.room = cn;
		} else {
			break;
		}
	}
	if (tidalwave_flag == 2) {
		tidalwave_flag = 0;
	}
	flag = 0;
	for (cn = 0; cn < bomblist.end; cn++) {
		bomb = bomblist.list[cn];
		if (bomb.enable == 1) {
			if (crash (self, bomb, 0.5)) {
				flag = 1;
				break;
			}
		}
	}
	if (hitpoint < 0) {
		crash_flag = 0;
	} else {
		crash_flag = flag;
	}
	if (crash_flag == 1) {
		double damage_in_this_flame;
		if (point < 300) {
			damage_in_this_flame = 0.5 * flame_step / lifespan;
		} else {
			damage_in_this_flame =
			    ((double) (point - 300) / 1000 +
			     1) * flame_step / lifespan;
		}
		hitpoint -= damage_in_this_flame;
		damage += damage_in_this_flame;
	}
	if (teapot.enable == 1) {
		teapot.z += teapot.speed * flame_step;
		if (teapot.z > roomsize_z / 2)
			teapot.enable = 0;
	}
	if (teapot.enable == 0 && metatime - teapot_time > 10) {
		teapot.enable = 1;
		teapot_time += 30;
		teapot.x = (rn () - 0.5) * roomsize_x;
		teapot.y = (rn () - 0.5) * roomsize_y;
		teapot.z = -roomsize_z / 2;
		teapot.speed = (rn () * 0.25 + 0.25) * speed;
	}
	if (teapot.enable == 1 && 0 < hitpoint) {
		if (crash (self, teapot, 0.6)) {
			teapot.enable = 0;
			hitpoint += (1.0 - hitpoint) * rn ();
			point += 100;
		}
	}
}
