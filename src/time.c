
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>

double wrap_time (void) {
	double time;
	struct timeval timeval;
	gettimeofday (&timeval, NULL);
	time = (timeval.tv_sec % 3600)
		+ ((double) timeval.tv_usec / 1000000);
	return time;
}
