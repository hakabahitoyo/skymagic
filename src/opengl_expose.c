
#include <GL/gl.h>
#include <GL/glut.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include "skymagic.h"

extern void RoundCube (double margin);
extern void glidcube (int div);

void drawStrokeString (char *string)
{
	int cn;
	void *font;
	font = GLUT_STROKE_ROMAN;
	for (cn = 0; string[cn] != '\0'; cn++) {
		glutStrokeCharacter (font, string[cn]);
	}
}


void draw_indicators () {
	int min;
	int width;
	int height;
	double rw;
	double rh;
	char buffer [1024];
	
	if (play_level == 0) {
		sprintf (buffer, "%d", point);
	} else {
		sprintf (buffer, "%d!", point);
	}

	float emission_life [4] = {1, 0, 0, 1};
	float emission_point [4] = {1, 1, 1, 1};
	float expletive [4] = {0, 0, 0, 1};

	width = window_width;
	height = window_height;
	min = width < height ? width : height;
	rw = (double) width / min;
	rh = (double) height / min;

	glMaterialfv (GL_FRONT, GL_DIFFUSE, expletive);
	glMaterialfv (GL_FRONT, GL_AMBIENT, expletive);

	glMatrixMode (GL_PROJECTION);
	glPushMatrix ();
		glLoadIdentity ();
		glOrtho (-rw, rw, -rh, rh, -1, 1);
		glMatrixMode (GL_MODELVIEW);
		glPushMatrix ();
			glLoadIdentity ();
			glPushMatrix ();
				glTranslated (-1, 0.8, 0);
				glScaled (hitpoint * 2.0, 0.015, 1);
				glTranslated (0.5, 0, 0);
				glMaterialfv (GL_FRONT, GL_EMISSION, emission_life);
			if (0 < hitpoint) {
				glutSolidCube (1);
			}
			glPopMatrix ();
			glPushMatrix ();
				glTranslated (-1, 0.85, 0);
				glScaled (1.2, 1, 1);
				glScaled (0.001, 0.001, 1);
				glMaterialfv (GL_FRONT, GL_EMISSION, emission_point);
				drawStrokeString (buffer);
			glPopMatrix ();
		glPopMatrix ();
	glMatrixMode (GL_PROJECTION);
	glPopMatrix ();
	glMatrixMode (GL_MODELVIEW);
}

void opengl_expose (void)
{
	double metatime = thetime;
	glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	draw_indicators ();

	int width = window_width;
	int height = window_height;
        int min;
        double rw, rh;
        min = width < height ? width : height;
        glViewport (0, 0, width, height);
        rw = (double) width / min;
        rh = (double) height / min;
        glMatrixMode (GL_PROJECTION);
        glLoadIdentity ();
        glFrustum (-rw, rw, -rh, rh, 3, 100);

	/* OpenGL機能の使用許可 */
	glEnable (GL_DEPTH_TEST);
	glEnable (GL_NORMALIZE);
	glEnable (GL_CULL_FACE);
	glEnable (GL_LIGHTING);
	glEnable (GL_LIGHT0);
	glEnable (GL_LIGHT1);

	/* 光源の減衰関数の設定 */
	glLightf (GL_LIGHT0, GL_CONSTANT_ATTENUATION, 0.75);
	glLightf (GL_LIGHT0, GL_LINEAR_ATTENUATION, 0.10);
	glLightf (GL_LIGHT0, GL_QUADRATIC_ATTENUATION, 0.05);
	glLightf (GL_LIGHT1, GL_CONSTANT_ATTENUATION, 0.75);
	glLightf (GL_LIGHT1, GL_LINEAR_ATTENUATION, 0.15);
	glLightf (GL_LIGHT1, GL_QUADRATIC_ATTENUATION, 0.25);

	/* シェーディングのための光源と素材の設定 */
	GLfloat light0[][4] =
	    { {0.4, 0.4, 0.2, 1}, {0.3, 0.3, 0.3, 1}, {0, 0, 0, 1} };
	GLfloat light1[][4] =
	    { {0.8, 0.2, 0.2, 1}, {0, 0, 0, 1}, {0, 0, 0, 1} };
	GLfloat oct_material[][4] =
	    { {0.8, 0.8, 0.5, 1}, {0.3, 0.3, 0.3, 1}, {0, 0, 0, 1} };
	GLfloat cube_material[][4] =
	    { {0.9, 0.3, 0.3, 1}, {0.6, 0.6, 0.6, 1}, {0, 0, 0, 1} };
	GLfloat flare_material[][4] = { {1.0, 0.2, 0.0, 1}, {0, 0, 0, 1} };
	GLfloat bomb_material[][4] = { {0.8, 0.8, 0.8, 1}, {0, 0, 0, 1} };
	GLfloat room_material[][3][4] = { {{0, 0, 0, 1}, {0, 0, 0, 1}, {0, 0.7, 1.0, 1}}, {{0.40, 0.40, 0.60, 1}, {0.30, 0.30, 0.45, 1}, {0, 0, 0, 1}} };	/* 視点の移動と回転 */

	glMatrixMode (GL_MODELVIEW);
	glLoadIdentity ();
	glTranslated (0, 0, -(viewdepth + swing_depth));
	glRotated (viewrotatex + swing_x, 1, 0, 0);
	glRotated (viewrotatey + swing_y, 0, 1, 0);
	glCullFace (GL_BACK);

	/* 敵キャラ（光源を含む）の描画 */
	glPushMatrix ();
	glTranslatef (light_x, light_y, light_z);
	glPushMatrix ();
	glMaterialfv (GL_FRONT, GL_EMISSION, oct_material[0]);
	glMaterialfv (GL_FRONT, GL_DIFFUSE, oct_material[1]);
	glMaterialfv (GL_FRONT, GL_AMBIENT, oct_material[2]);
	glRotatef (metatime * 360, 0, 1, 0);
	glScalef (0.5, 0.5, 0.5);
	glutSolidOctahedron ();
	glPopMatrix ();
	glPushMatrix ();
	glLightfv (GL_LIGHT0, GL_DIFFUSE, light0[0]);
	glLightfv (GL_LIGHT0, GL_AMBIENT, light0[1]);
	glLightfv (GL_LIGHT0, GL_POSITION, light0[2]);
	glPopMatrix ();
	glPopMatrix ();

	/* 操作キャラ（光源を含む）の描画 */
	glPushMatrix ();
	glTranslatef (self.x, self.y, self.z);
	if (hitpoint >= 0) {
		glPushMatrix ();
		glMaterialfv (GL_FRONT, GL_EMISSION, cube_material[0]);
		glMaterialfv (GL_FRONT, GL_DIFFUSE, cube_material[1]);
		glMaterialfv (GL_FRONT, GL_AMBIENT, cube_material[2]);
		glScalef (self_size, self_size, self_size);
		RoundCube (0.125);
		glPopMatrix ();
	}
	glPushMatrix ();
	glLightfv (GL_LIGHT1, GL_DIFFUSE, light1[0]);
	glLightfv (GL_LIGHT1, GL_AMBIENT, light1[1]);
	glLightfv (GL_LIGHT1, GL_POSITION, light1[2]);
	glPopMatrix ();
	if (crash_flag == 1) {
		glPushMatrix ();
		glMaterialfv (GL_FRONT, GL_EMISSION, flare_material[0]);
		glMaterialfv (GL_FRONT, GL_AMBIENT_AND_DIFFUSE,
			      flare_material[1]);
		glRotatef (90, 1, 0, 0);
		glRotatef (360 * metatime, 0, 0, 1);
		glutWireSphere (1, 18, 12);
		glPopMatrix ();
	}
	glPopMatrix ();

	/* 弾丸の描画 */
	glMaterialfv (GL_FRONT, GL_AMBIENT_AND_DIFFUSE, bomb_material[0]);
	glMaterialfv (GL_FRONT, GL_EMISSION, bomb_material[1]);
	struct bomb bomb;
	int cn;
	for (cn = 0; cn < bomblist.end; cn++) {
		bomb = bomblist.list[cn];
		if (bomb.enable == 1) {
			glPushMatrix ();
			glTranslatef (bomb.x, bomb.y, bomb.z);
			glutSolidSphere (0.2, 16, 8);
			glPopMatrix ();
		}
	}

	/* 急須の描画 */
	if (teapot.enable == 1) {
		glPushMatrix ();
		glTranslatef (teapot.x, teapot.y, teapot.z);
		glRotatef (metatime * 180, 0, 1, 0);
		glCullFace (GL_FRONT);
		glutSolidTeapot (0.4);
		glCullFace (GL_BACK);
		glPopMatrix ();
	}

	/* 空間の描画 */
	glPushMatrix ();
	glScalef (roomsize_x, roomsize_y, roomsize_z);
	glMaterialfv (GL_FRONT, GL_DIFFUSE,
		      room_material[room_modeling][0]);
	glMaterialfv (GL_FRONT, GL_AMBIENT,
		      room_material[room_modeling][1]);
	glMaterialfv (GL_FRONT, GL_EMISSION,
		      room_material[room_modeling][2]);
	if (room_modeling == 0) {
		glutWireCube (1);
	}
	if (room_modeling == 1) {
		glCullFace (GL_FRONT);
		glidcube (8);
		glCullFace (GL_BACK);
	}
	glPopMatrix ();
}
