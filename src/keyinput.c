
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <SDL/SDL.h>
#include "skymagic.h"

#define ROTATE_SPEED 30
#define DEPTH_SPEED 6

extern double max (double a, double b);
extern double min (double a, double b);

/*-- キーボード入力 --*/
void key_input (void)
{
	Uint8 *snap;
	int n;
	SDLMod modifire;

	double speedup;
	double swing_speedup;
	int exclusive;
	int swing;

	snap = SDL_GetKeyState (&n);
	modifire = SDL_GetModState ();

	speedup = 1.0;
	if (0 == crash_flag && (modifire & KMOD_SHIFT)) {
		speedup = 2.0;
	}
	swing_speedup = max (1.5, speedup);
	swing = 0;
	exclusive = 0;

	if (0 == exclusive && (modifire & KMOD_CTRL)) {
		swing = 1;
		exclusive = 1;
		if (SDLK_d < n && snap[SDLK_d]) {
			viewdepth += DEPTH_SPEED * speedup * flame_step;
			viewdepth = min (48, viewdepth);
		}
		if (SDLK_f < n && snap[SDLK_f]) {
			viewdepth -= DEPTH_SPEED * speedup * flame_step;
			viewdepth = max (0.5, viewdepth);
		}
		if (SDLK_j < n && snap[SDLK_j]) {
			viewrotatey -= ROTATE_SPEED * speedup * flame_step;
		}
		if (SDLK_k < n && snap[SDLK_k]) {
			viewrotatex += ROTATE_SPEED * speedup * flame_step;
		}
		if (SDLK_i < n && snap[SDLK_i]) {
			viewrotatex -= ROTATE_SPEED * speedup * flame_step;
		}
		if (SDLK_l < n && snap[SDLK_l]) {
			viewrotatey += ROTATE_SPEED * speedup * flame_step;
		}
	}
	if (0 == exclusive && (modifire & KMOD_ALT)) {
		exclusive = 1;
		if (SDLK_d < n && snap[SDLK_d]) {
			swing = 1;
			swing_direction = DIRECTION_BACK;
			swing_depth += DEPTH_SPEED * speedup * flame_step;
		}
		if (SDLK_f < n && snap[SDLK_f]) {
			swing = 1;
			swing_direction = DIRECTION_FORWARD;
			swing_depth -= DEPTH_SPEED * speedup * flame_step;
		}
		if (SDLK_j < n && snap[SDLK_j]) {
			swing = 1;
			swing_direction = DIRECTION_LEFT;
			swing_y -= ROTATE_SPEED * speedup * flame_step;
		}
		if (SDLK_k < n && snap[SDLK_k]) {
			swing = 1;
			swing_direction = DIRECTION_DOWN;
			swing_x += ROTATE_SPEED * speedup * flame_step;
		}
		if (SDLK_i < n && snap[SDLK_i]) {
			swing = 1;
			swing_direction = DIRECTION_UP;
			swing_x -= ROTATE_SPEED * speedup * flame_step;
		}
		if (SDLK_l < n && snap[SDLK_l]) {
			swing = 1;
			swing_direction = DIRECTION_RIGHT;
			swing_y += ROTATE_SPEED * speedup * flame_step;
		}
	}
	if (0 == exclusive && SDLK_SPACE < n && snap[SDLK_SPACE]) {
		swing = 1;
		switch (swing_direction) {
		case DIRECTION_LEFT:
			swing_y -=
			    ROTATE_SPEED * speedup * flame_step;
			break;
		case DIRECTION_RIGHT:
			swing_y +=
			    ROTATE_SPEED * speedup * flame_step;
			break;
		case DIRECTION_UP:
			swing_x -=
			    ROTATE_SPEED * speedup * flame_step;
			break;
		case DIRECTION_DOWN:
			swing_x +=
			    ROTATE_SPEED * speedup * flame_step;
			break;
		case DIRECTION_FORWARD:
			swing_depth -=
			    DEPTH_SPEED * speedup * flame_step;
			break;
		case DIRECTION_BACK:
			swing_depth +=
			    DEPTH_SPEED * speedup * flame_step;
			break;
		}
	}
	if (0 == exclusive) {
		if (SDLK_d < n && snap[SDLK_d]) {
			self.z += self.speed * speedup * flame_step;
			self.z =
			    min (self.z, (roomsize_z - self_size) / 2);
		}
		if (SDLK_f < n && snap[SDLK_f]) {
			self.z -= self.speed * speedup * flame_step;
			self.z =
			    max (self.z, -(roomsize_z - self_size) / 2);
		}
		if (SDLK_j < n && snap[SDLK_j]) {
			self.x -= self.speed * speedup * flame_step;
			self.x =
			    max (self.x, -(roomsize_x - self_size) / 2);
		}
		if (SDLK_k < n && snap[SDLK_k]) {
			self.y -= self.speed * speedup * flame_step;
			self.y =
			    max (self.y, -(roomsize_y - self_size) / 2);
		}
		if (SDLK_i < n && snap[SDLK_i]) {
			self.y += self.speed * speedup * flame_step;
			self.y =
			    min (self.y, (roomsize_y - self_size) / 2);
		}
		if (SDLK_l < n && snap[SDLK_l]) {
			self.x += self.speed * speedup * flame_step;
			self.x =
			    min (self.x, (roomsize_x - self_size) / 2);
		}
	}
	if (0 == swing) {
		if (swing_x < 0.001) {
			swing_x +=
			    ROTATE_SPEED * swing_speedup * flame_step;
			if (0 < swing_x)
				swing_x = 0;
		}
		if (-0.001 < swing_x) {
			swing_x -=
			    ROTATE_SPEED * swing_speedup * flame_step;
			if (swing_x < 0)
				swing_x = 0;
		}
		if (swing_y < 0.001) {
			swing_y +=
			    ROTATE_SPEED * swing_speedup * flame_step;
			if (0 < swing_y)
				swing_y = 0;
		}
		if (-0.001 < swing_y) {
			swing_y -=
			    ROTATE_SPEED * swing_speedup * flame_step;
			if (swing_y < 0)
				swing_y = 0;
		}
		if (swing_depth < 0.001) {
			swing_depth +=
			    DEPTH_SPEED * swing_speedup * flame_step;
			if (0 < swing_depth)
				swing_depth = 0;
		}
		if (-0.001 < swing_depth) {
			swing_depth -=
			    DEPTH_SPEED * swing_speedup * flame_step;
			if (swing_depth < 0)
				swing_depth = 0;
		}
	}
}
